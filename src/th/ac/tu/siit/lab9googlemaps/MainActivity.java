package th.ac.tu.siit.lab9googlemaps;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;

public class MainActivity extends FragmentActivity
	implements ConnectionCallbacks,OnConnectionFailedListener, LocationListener {
	
	GoogleMap map;
	Location currentLocation;
	LocationClient locClient;
	LocationRequest locRequest;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		MapFragment mapFragment = 
				(MapFragment)getFragmentManager().findFragmentById(R.id.map);
		map = mapFragment.getMap();
		map.setMyLocationEnabled(true);
		map.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
		locClient = new LocationClient(this, this, this);
		
	}
	
	protected void onStart() {
		super.onStart();
		locClient.connect();
	}
	
	protected void onStop() {
		locClient.disconnect();
		super.onStop();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onConnectionFailed(ConnectionResult arg0) {
		
	}

	@Override
	public void onConnected(Bundle arg0) {
		currentLocation = locClient.getLastLocation();

		locRequest = LocationRequest.create();
		locRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
		locRequest.setInterval(5*1000);
		locRequest.setFastestInterval(1*1000);
		locClient.requestLocationUpdates(locRequest, this);
	}

	@Override
	public void onDisconnected() {
		
	}

	@Override
	public void onLocationChanged(Location l) {
		if (currentLocation.distanceTo(l) > 5) {
			currentLocation = l;
			
			MarkerOptions m = new MarkerOptions();
			m.position(new LatLng(l.getLatitude(), l.getLongitude()));
			map.addMarker(m);
		}
	}
}
